<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'address',
        'title',
        'image',
        'email',
        'phone',
        'about_us',
        'facebook',
        'meta_img',
        'meta_desc',
        'logo_white',
        'meta_title',
        'key_words',
        'lng',
        'lat'
];



}
