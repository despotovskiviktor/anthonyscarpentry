<?php


namespace App\Http\Controllers\ViewComposer;


use App\Models\Testimonial;
use Illuminate\View\View;

class TestimonialComposer
{
    public function compose(View $view)
    {
        $view->with('testimomnial', Testimonial::first());
    }

}
