<?php


namespace App\Http\Controllers\ViewComposer;

use App\Models\User;
use Illuminate\View\View;

class UserComposer
{
    public function compose(View $view)
    {
        $view->with('user', User::first());
    }
}
