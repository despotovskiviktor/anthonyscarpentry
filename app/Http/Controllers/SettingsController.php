<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Helper\ImageStore;


class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::first();
        if ($settings) {
            return view('settings.index')->with(['settings' => $settings]);
        }
        return view('settings.create');
    }

    public function create()
    {
        return view('settings.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'title' => ['required', 'string', 'min:8'],
            'about_us' => ['required', 'string', 'min:8'],
            'facebook' => ['required', 'string', 'min:8'],
            'meta_desc' => ['required', 'string', 'min:8'],
            'meta_title' => ['required', 'string', 'min:8'],
            'key_words' => ['required', 'string', 'min:8'],
            'meta_img' => ['required', 'image'],
            'logo_white' => ['required', 'image'],
            'phone' => ['required', 'string', 'min:8'],
            'image' => ['required', 'image'],
        ]);

        if ($validator->fails()) {

            return redirect('settings/create')
                ->withErrors($validator)
                ->withInput();
        }


        $imageObj = new ImageStore($request, 'settings');
        $image = $imageObj->imageStore();

        $metaImgObj = new ImageStore($request, 'settings');
        $meta_image = $metaImgObj->imageStore('meta_img');


        $logoWhiteObj = new ImageStore($request, 'settings');
        $logo_white = $logoWhiteObj->imageStore('logo_white');


        Settings::create([
            'url' => $request->get('url'),
            'address' => $request->get('address'),
            'facebook' => $request->get('facebook'),
            'email' => $request->get('email'),
            'about_us' => $request->get('about_us'),
            'image' => $image,
            'meta_img' => $meta_image,
            'about_us' => $request->get('about_us'),
            'meta_desc' => $request->get('meta_desc'),
            'meta_title' => $request->get('meta_title'),
            'key_words' => $request->get('key_words'),
            'logo_white' => $logo_white,
            'phone' => $request->get('phone'),
            'title' => $request->get('title'),
            'lng' => $request->get('lng'),
            'lat' => $request->get('lat')
        ]);

        Session::flash('flash_message', 'Settings successfully created!');
        return redirect()->back();
    }

    public function edit()
    {
        $settings = Settings::first();
        $data = ['settings' => $settings];
        return view('settings.edit')->with($data);
    }

    public function update(Request $request, $id)
    {

        $settings = Settings::FindOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $imageObj = new ImageStore($request, 'settings');
            $image = $imageObj->imageStore();
            $input['image'] = $image;
        }
        if ($request->hasFile('meta_img')) {

            $metaImgObj = new ImageStore($request, 'settings');
            $meta_image = $metaImgObj->imageStore('meta_img');
            $input['meta_img'] = $meta_image;
        }
        if ($request->hasFile('logo_white')) {

            $logoWhiteObj = new ImageStore($request, 'settings');
            $logo_white = $logoWhiteObj->imageStore('logo_white');
            $input['logo_white'] = $logo_white;
        }


        $settings->fill($input)->save();

        return view('settings.index')->with(['settings' => $settings]);
    }

    public function destroy($id)
    {
        //
    }
}
