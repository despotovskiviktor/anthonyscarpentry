<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{

    public function index()
    {
        $testimonials = Testimonial::all();
        $data = ['testimonials' => $testimonials];
        return view('testimonial.index')->with($data);
    }

    public function create()
    {
        return view('testimonial.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'desc' => ['required', 'string'],
        ]);

        if ($validator->fails()) {

            return redirect('testimonial/create')
                ->withErrors($validator)
                ->withInput();
        }
        Testimonial::create([
            'name'         => $request->get('name'),
            'company_name' => $request->get('company_name'),
            'desc'         => $request->get('desc'),
        ]);

        $testimonials = Testimonial::all();
        $data = ['testimonials' => $testimonials];
        return view('testimonial.index')->with($data);

    }


    public function edit($id)
    {
        $testimonial = Testimonial::FindOrFail($id);
        $data = ['testimonial' => $testimonial];
        return view('testimonial.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::FindOrFail($id);
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string']
        ]);

        if ($validator->fails()) {

            return redirect('admin/testimonial/create')
                ->withErrors($validator)
                ->withInput();
        }
        $testimonial->update([
            'name'         => $request->get('name'),
            'company_name' => $request->get('company_name'),
            'desc'         => $request->get('desc'),
        ]);

        $testimonial = Testimonial::all();
        $data = ['testimonials' => $testimonial];
        return view('testimonial.index')->with($data);
    }

    public function destroy($id)
    {
        $testimonial = Testimonial::FindOrFail($id);
        $testimonial->delete();
        $testimonials = Testimonial::all();
        $data = ["testimonials" => $testimonials];
        return view('testimonial.index')->with($data);
    }
}
