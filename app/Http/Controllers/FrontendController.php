<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Gallery;
use App\Models\Settings;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Mail\ContactForm;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index()
    {
        $settings = Settings::first();
        $testimonials = Testimonial::all();
        $categories = Categories::all();
        $catTree = Categories::getTreeHp();
        $catTreeMobile = Categories::getTreeMobile();
        $galleryTree = Categories::renderGallery();
        $galleryTreeMobile = Categories::getGalleryMobile();

        $gallery = Gallery::where('front_page', '=', '1')->get()->shuffle();
        $data = [
            'settings'          => $settings,
            'testimonials'      => $testimonials,
            'categories'        => $categories,
            'catTree'           => $catTree,
            'catTreeMobile'     => $catTreeMobile,
            'gallery'           => $gallery,
            'galleryTree'       => $galleryTree,
            'galleryTreeMobile' => $galleryTreeMobile
        ];
        return view('frontend.index')->with($data);
    }
    public function sendMessage(Request $request)
    {
        $settings = Settings::first();
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $message = $request->get('message');
        $mail = Mail::to($settings->email)->send(new ContactForm($message, $name, $phone, $email));
        return response()->json(["status" => "success"], 200);
    }

    public function category($slug)
    {
        $settings = Settings::first();
        $testimonials = Testimonial::all();
        $categories = Categories::all();
        $catTree = Categories::getTreeHp();
        $catTreeMobile = Categories::getTreeMobile();
        $gallery = Gallery::where('front_page', '=', '1')->get()->shuffle();
        $galleryTree = Categories::renderGallery();
        $category = Categories::where('slug', '=', $slug)->first();
        $galleryTreeMobile = Categories::getGalleryMobile();

        $data = [
            'settings'          => $settings,
            'testimonials'      => $testimonials,
            'categories'        => $categories,
            'catTree'           => $catTree,
            'catTreeMobile'     => $catTreeMobile,
            'gallery'           => $gallery,
            'category'          => $category,
            'galleryTree'       => $galleryTree,
            'galleryTreeMobile' => $galleryTreeMobile
        ];
        return view('frontend.category')->with($data);
    }

}
