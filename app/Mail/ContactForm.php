<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $formMessage;
    public $formName;
    public $formPhone;
    public $formEmail;

    public function __construct($message, $name, $phone, $email)
    {
        $this->formMessage = $message;
        $this->formName = $name;
        $this->formEmail = $email;
        $this->formPhone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'formMessage' => $this->formMessage,
            'formName'    => $this->formName,
            'formEmail'   => $this->formEmail,
            'formPhone'   => $this->formPhone
            ];

        return $this->markdown('vendor.mail.html.message')->with($data);
    }
}
