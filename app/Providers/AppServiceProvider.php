<?php

namespace App\Providers;


use App\Http\Controllers\ViewComposer\SettingsComposer;
use App\Http\Controllers\ViewComposer\TestimonialComposer;
use App\Http\Controllers\ViewComposer\UserComposer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer('layouts.app', SettingsComposer::class);
        View::composer('layouts.app', TestimonialComposer::class);
        View::composer('layouts.app', UserComposer::class);
        Schema::defaultStringLength(191);
    }
}
