<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
        'url' => 'http://anthonyscarpentry.test/',
        'address' => 'oslo 7',
        'title' => 'Anthony\'s Carpentry',
        'image' => '',
        'logo_white' => '',
        'email' => 'martin@myhost.mk',
        'phone' => '+38971213980',
        'about_us' => 'Short desc',
        'facebook' => 'http://fb.com/anthonyscarpentry',
        'meta_img' => '',
        'meta_title' => 'Anthonys Capentry',
        'meta_desc' => 'Some desc',
    ]);
    }
}
