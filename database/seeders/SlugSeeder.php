<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categories;
use Illuminate\Support\Str;

class SlugSeeder extends Seeder
{
    public function run()
    {
        $categories = Categories::all();
        foreach($categories as $category)
        {
            $category->slug = Str::slug($category->name);
            $category->save();
        }
    }
}
