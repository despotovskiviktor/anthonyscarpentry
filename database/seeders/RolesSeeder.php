<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        Roles::create(['name' => 'Administrator']);
        $role = new Roles();
        $role->name = 'User';
        $role->save();
        Roles::create(['name' => 'Guest']);

    }
}
