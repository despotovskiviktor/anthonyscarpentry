<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FrontPageInGallery extends Migration
{
    public function up()
    {
        Schema::table('gallery', function (Blueprint $table) {
            $table->boolean('front_page')->after('cat_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('gallery', function (Blueprint $table) {
            $table->dropColumn(['front_page']);
        });
    }
}
