@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif
            <div class="col-12">
                <h1 style="text-align: center">Add images to {{ $category->name }}</h1>
                <hr>
                <form action="{{ route('gallery.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="cat_id" value="{{ $category->id }}">
                    <div class="form-group row">
                        <label for="image" class="col-md-4 col-form-label text-md-right @error('images') is-invalid @enderror">Upload image</label>
                        <div class="col-md-6">
                            <input id="image" type="file" name="images[]" multiple>
                            @error('image')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-fill btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

