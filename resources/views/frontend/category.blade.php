@extends('layouts.frontend')
@section('content')
    <div class="project-area space__bottom--r120 space__top--50" id="photos">
        <div class="isotope-container project-wrapper space__bottom--m5" id="project-justify-wrapper">
            @foreach($category->gallery as $image)
                <div class="single-project-wrapper isotope-item cat-{{ $image->cat_id }}">
                    <a class="single-project-item popup-img" href="/assets/img/gallery/originals/{{ $image->image }}">
                        <img src="/assets/img/gallery/medium/{{ $image->image }}" class="img-fluid" alt="">
                        <span class="single-project-title">{{ $image->categories->name }}</span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
