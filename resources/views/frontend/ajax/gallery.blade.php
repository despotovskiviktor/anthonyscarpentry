@foreach($gallery as $image)
    <div class="single-project-wrapper isotope-item cat-{{ $image->cat_id }}">
        <a class="single-project-item popup-img" href="/assets/img/gallery/originals/{{ $image->image }}">
            <img src="/assets/img/gallery/medium/{{ $image->image }}" class="img-fluid" alt="">
            <span class="single-project-title">{{ $image->categories->name }}</span>
        </a>
    </div>
@endforeach


