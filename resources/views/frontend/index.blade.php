@extends('layouts.frontend')
@section('content')
    <!--====================  about area ====================-->
    <div class="about-area space__bottom--r120" id="aboutUs">
        <div class="container">
            <div class="row align-items-center row-25">
                <div class="col-md-6">
                    <!-- service banner -->
                    <div class="service-banner space__bottom__md--40 space__bottom__lm--40">
                        <img src="/images/carpentry-man.png" class="float-none float-lg-left" alt=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-content">
                        <!-- section title -->
                        <div class="section-title space__bottom--25">
                            <h3 class="section-title__sub">Since 1999</h3>
                            <h2 class="section-title__title">We are dedicated to providing best construction and
                                service</h2>
                        </div>
                        <p class="about-content__text space__bottom--40">{!! strip_tags($settings->about_us) !!}</p>
                        <a href="#filters" class="default-btn">Start now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of about area  ====================-->
    <!--====================  feature area ====================-->
    <div class="feature-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-2 order-lg-1">
                    <!-- feature content wrapper -->
                    <div class="feature-content-wrapper space__bottom--m35">
                        <div class="single-feature space__bottom--35">
                            <div class="single-feature__icon">
                                <img src="/frontend/assets/img/icons/feature-1.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Quality Service</h4>
                                <p class="single-feature__text">We pay close attention to what our clients want and need so
                                    that their home or business represents them perfectly. We value communication and will
                                    clarify each phase of the project, from start to finish.
                                </p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--35">
                            <div class="single-feature__icon">
                                <img src="/frontend/assets/img/icons/feature-2.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">First-class Workmanship</h4>
                                <p class="single-feature__text">Our extensive knowledge in the remodeling industry allows us
                                    to provide you with unsurpassed craftsmanship and attention to details. Our technicians
                                    have many years of experience and are fully insured for your safety.
                                </p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--35">
                            <div class="single-feature__icon">
                                <img src="/frontend/assets/img/icons/feature-3.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Affordable Price</h4>
                                <p class="single-feature__text">We combine quality workmanship, superior knowledge and
                                    affordable prices to provide you with service unmatched by our competitors and always
                                    within your budget. </p>
                            </div>
                        </div>
                        <a href="#contact-form" class="default-btn">Contact Us</a>
                    </div>
                </div>
                <div class="col-lg-6 space__bottom__md--40 space__bottom__lm--40 order-1 order-lg-2">
                    <!-- feature content image -->
                    <div class="feature-content-image">
                        <img src="/images/small2.png" class="img-fluid" alt="">
                        <img src="/images/small.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of feature area  ====================-->
    <!--====================  service area ====================-->
    <div class="service-area" id="services">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 mt-0 mt-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto" id="services">
                        <h3 class="section-title__sub">Our Services</h3>
                        <h2 class="section-title__title">Unique Designs and Quality Service Makes Clients Happy</h2>
                    </div>
                    <!-- service slider -->
                    <div class="row service-slider-wrapper space__bottom__md--40 space__bottom__lm--40">
                        @foreach($categories as $category)
                            <div class="col single-service text-center">
                                <div class="single-service__image space__bottom--15">
                                    <div class="service-image"
                                         alt="{{$category->name}}"
                                         style="background-image: url('/assets/img/categories/medium/{{$category->image}}')"></div>
                                </div>
                                <h4 class="single-service__content">
                                    <a href="/{{ $category->slug }}#photos" data-filter-id="{{ $category->id }}">{{$category->name}}</a>
                                </h4>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of service area  ====================-->
    <!--====================  project area ====================-->
    <div class="project-area space__bottom--r120 space__top--50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Our Projects</h3>
                        <h2 class="section-title__title">Here you find our latest projects, from Residential to Commercial renovations:
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="filters" id="filters">
            <ul class="nav nav-pills">
                <li class="active">
                    <button class="default-btn" data-filter="*">All</button>
                </li>
                @foreach($categories as $category)
                    <li class="">
                        <button id="cat-{{$category->id}}" class="default-btn"
                                data-filter=".cat-{{$category->id}}">{{ $category->name }}</button>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="isotope-container project-wrapper space__bottom--m5" id="project-justify-wrapper">
            @foreach($gallery as $image)
                <div class="single-project-wrapper isotope-item cat-{{ $image->cat_id }}">
                    <a class="single-project-item popup-img" href="/assets/img/gallery/originals/{{ $image->image }}">
                        <img src="/assets/img/gallery/medium/{{ $image->image }}" class="img-fluid" alt="">
                        <span class="single-project-title">{{ $image->categories->name }}</span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="project-wrapper space__bottom--m5" id="gallery-images"></div>
    <!--====================  fun fact area ====================-->
    <div class="fun-fact-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- fun fact wrapper -->
                    <div class="fun-fact-wrapper fun-fact-wrapper-bg bg-img" data-bg="/images/metro1.png">
                        <div
                            class="fun-fact-inner background-color--default-overlay background-repeat--x-bottom space__inner--y30 bg-img"
                            data-bg="/frontend/assets/img/icons/ruler-black.png">
                            <div class="fun-fact-content-wrapper">
                                <div class="single-fun-fact">
                                    <h3 class="single-fun-fact__number counter">300</h3>
                                    <h4 class="single-fun-fact__text">Projects</h4>
                                </div>
                                <div class="single-fun-fact">
                                    <h3 class="single-fun-fact__number counter">240</h3>
                                    <h4 class="single-fun-fact__text">Clients</h4>
                                </div>
                                <div class="single-fun-fact">
                                    <h3 class="single-fun-fact__number counter">22</h3>
                                    <h4 class="single-fun-fact__text">Years</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of fun fact area  ====================-->

    <!--====================  End of project area  ====================-->
    <!--====================  testimonial cta area ====================-->
    <div class="testimonial-cta-area space__bottom--r120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 space__bottom__md--40 space__bottom__lm--40">
                    <!-- cta block -->
                    <div class="cta-block cta-block--shadow cta-block--bg bg-img"
                         data-bg="/images/testimonial1.jpg">
                        <div class="cta-block__inner background-color--default-light-overlay space__inner--ry100">
                            <p class="cta-block__semi-bold-text text-center">Do you have a project?
                                <br> Just dial:
                            </p>
                            <p class="cta-block__bold-text text-center" a
                               href="{{ $settings->phone }}">{{$settings->phone}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Testimonials</h3>
                        <h2 class="section-title__title">What clients say</h2>
                    </div>
                    <!-- testimonial slider -->
                    <div class="testimonial-slider-wrapper space__inner__bottom__md--30  space__inner__bottom__lm--30">
                        @foreach( $testimonials as $testimonial)
                            <div class="single-testimonial text-center">
                                <p class="single-testimonial__text space__bottom--40">
                                    {!!  strip_tags($testimonial->desc)  !!}
                                <div class="single-testimonial__rating space__bottom--10">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <h5 class="single-testimonial__author">{{$testimonial->name}}</h5>
                                <p class="single-testimonial__author-des">{{$testimonial->company_name}}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of testimonial cta area  ====================-->

    <div class="conact-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12">
                    <div class="contact-form" id="contact-form">
                        <h4 class="space__bottom--30" style="text-align: center">GET IN TOUCH</h4>
                        <h5 class="space__bottom--30" style="text-align: center; color: grey; ">Do you have a question or
                            wish to talk about your next project? Feel free to give us a ring or drop us a note here!</h5>
                        <form id="contact-mail" action="" method="post">
                            <div class="row row-10">
                                <div class="col-md-4 col-12 space__bottom--20"><input name="con_name" required type="text"
                                                                                      placeholder="Name*"
                                                                                      style="text-align: center"></div>
                                <div class="col-md-4 col-12 space__bottom--20"><input name="con_phone" required type="text"
                                                                                      placeholder="Phone*"
                                                                                      style="text-align: center"></div>
                                <div class="col-md-4 col-12 space__bottom--20"><input name="con_email" required type="email"
                                                                                      placeholder="Email*"
                                                                                      style="text-align: center"></div>
                                <div class="col-12"><textarea name="con_message" required placeholder="Write your message here..."
                                                              style="text-align: center"></textarea></div>
                                <div class="col-12">
                                    <button>Send Message</button>
                                </div>
                            </div>
                        </form>
                        <p class="form-message"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
