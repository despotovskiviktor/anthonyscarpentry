@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-12">
                <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="image" class="col-md-4 col-form-label text-md-right">Upload image</label>
                        <div class="col-md-6">
                            <input id="image" type="file" name="image">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Category Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" autocomplete="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="meta_desc" class="col-md-4 col-form-label text-md-right">Category Description</label>
                        <div class="col-md-6">
                            <textarea id="catDesc" type="text" class="form-control" name="meta_desc" required></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="parent_id" class="col-md-4 col-form-label text-md-right">Subcategory</label>
                        <div class="col-md-6">
                            <select class="form-control @error('parent_id') is-invalid @enderror" name="parent_id">
                                <option value="">Main Category</option>
                                {!! $categories !!}
                            </select>
                            @error('parent_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success glyphicon glyphicon-ok"></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

@endsection
@section('scripts')
    <script>
        CKEDITOR.replace('catDesc');
    </script>
@endsection
