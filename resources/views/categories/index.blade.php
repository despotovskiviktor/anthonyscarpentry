@extends('layouts.app')

@section('content')

    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 ">
        <div class="btn-group pull-right">
            <a href="/admin/categories/create" class="btn btn-success">+ Category</a>
        </div>
        {!! $categories !!}
    </div>
@endsection
