@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">URL</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->url }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Title</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->title }}
                    </p><br>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Meta Title</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->meta_title }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Address</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->address }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Email</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->email }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Logo</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        <img src="/assets/img/settings/thumbnails/{{ $settings->image }}">
                    </p><br>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Logo White</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        <img src="/assets/img/settings/thumbnails/{{ $settings->logo_white }}">
                    </p><br>
                </div>
            </div>
            <!-- END INPUT SIZING -->
        </div>
        <div class="col-md-6">
            <!-- LABELS -->
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Phone Number</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->phone }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Facebook</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->facebook }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">About us</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->about_us }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Keywords</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->key_words }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Description</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        {{ $settings->meta_desc }}
                    </p><br>
                </div>
            </div>
            <div class="panel panel-filled">
                <div class="panel-heading">
                    <h3 class="panel-title">Meta Image</h3>
                </div>
                <div class="panel-body">
                    <p class="demo-button">
                        <img src="/assets/img/settings/thumbnails/{{ $settings->meta_img }}">
                    </p><br>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div id="map" style="width: 100%; height: 400px"></div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
// Google Maps
            let latitude;
            let longitude;

            latitude =  @if($settings->lat) {{ $settings->lat }} @else 48.85 @endif;
            longitude = @if($settings->lng) {{$settings->lng}} @else 2.35 @endif;

            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: latitude, lng: longitude},
                zoom: 15
            });

            var marker = new google.maps.Marker({
                position: {lat: latitude, lng: longitude},
                map: map,
                draggable: true
            });

        });
    </script>
@endsection

