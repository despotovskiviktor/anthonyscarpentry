@extends('layouts.app')

@section('content')
        <div class="row">
                <div class="col-12">
                    <form action="{{ route('settings.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">Upload Image</label>
                            <div class="col-md-6">
                                <input id="image" type="file"  name="image">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="logo_white" class="col-md-4 col-form-label text-md-right">Upload Logo White</label>
                            <div class="col-md-6">
                                <input id="logo_white" type="file"  name="logo_white">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="meta_img" class="col-md-4 col-form-label text-md-right">Upload Meta Image</label>
                            <div class="col-md-6">
                                <input id="meta_img" type="file"  name="meta_img">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="url" class="col-md-4 col-form-label text-md-right">URL</label>
                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror"
                                       value="{{ old('url') }}" name="url" autocomplete="url" required autofocus>
                                @error('url')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                                       value="{{ old('title') }}" name="title" required>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="meta_title" class="col-md-4 col-form-label text-md-right">Meta Title</label>
                            <div class="col-md-6">
                                <input id="meta_title" type="text" class="form-control @error('meta_title') is-invalid @enderror"
                                       value="{{ old('meta_title') }}" name="meta_title" required>
                                @error('meta_title')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="key_words" class="col-md-4 col-form-label text-md-right">Keywords</label>
                            <div class="col-md-6">
                                <input id="key_words" type="text" class="form-control @error('key_words') is-invalid @enderror"
                                       value="{{ old('key_words') }}" name="meta_title" required>
                                @error('key_words')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror"
                                       name="address" value="{{ old('address') }}" required>
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="text"
                                       class="form-control @error('email') is-invalid @enderror" name="email" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook</label>
                            <div class="col-md-6">
                                <input id="facebook" type="text"
                                       class="form-control @error('facebook') is-invalid @enderror" name="facebook" required>
                                @error('facebook')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone Number</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="about_us" class="col-md-4 col-form-label text-md-right">About Us</label>
                            <div class="col-md-6">
                                <textarea id="about1" type="text" class="form-control" name="about_us" required></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="meta_desc" class="col-md-4 col-form-label text-md-right">Meta Description</label>
                            <div class="col-md-6">
                                <textarea id="meta_desc" type="text" class="form-control" name="meta_desc" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input id="searchmap" class="controls" type="text" placeholder="Search Box"
                                   style="width: 400px; height: 35px;"/>
                            <div id="map" style="width: 100%; height: 400px">
                            </div>
                        </div>

                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="lng" id="lng">

                        <div class="form-group row">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-fill btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
// Google Maps
            map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 41.9991801, lng: 21.3880133},
            zoom: 15
        });

        var marker = new google.maps.Marker({
            position: {lat: 41.9991801, lng: 21.3880133},
            map: map,
            draggable: true
        });
        var input = document.getElementById('searchmap');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);
        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;
            for (i = 0; place = places[i]; i++) {
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }
            map.fitBounds(bounds);
            map.setZoom(8);
        });
        google.maps.event.addListener(marker, 'position_changed', function () {
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#lat').val(lat);
            $('#lng').val(lng);
        });
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                $("#searchmap").attr('value');
                //add more buttons here
                return false;
            }
        });
    });
</script>
@endsection

