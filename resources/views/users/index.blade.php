@extends('layouts.app')

@section('content')
    <div class="col-12">

    <div class="panel panel-filled">
        <div class="panel-heading">
            <h3 style="text-align: center">Users</h3>
        </div>
        <div class="panel-body">
            <table class="table table-hover table-striped table-responsive-sm">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Full Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created At</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <td><img class="img-rounded" width="50x" height="50px"
                                 src="/assets/img/users/thumbnails/{{ $user->image }}"
                                 alt="{{$user->first_name}}.{{ $user->last_name }}">
                        </td>
                        <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td><a href="/admin/user/{{ $user->id }}/edit" class="btn btn-warning btn-circle"><i
                                    class="glyphicon glyphicon-edit"></i></a></td>
                        <td>
                            <form action="{{ route('user.destroy', $user) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-circle"><i
                                        class="glyphicon glyphicon-remove"></i></button>
                            </form>
                        </td>
                <tr/>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
