@extends('layouts.app')

@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            {{ Session::get('flash_message') }}
        </div>
        @endif
        </form>
        <div class="col-12">
            <form action="{{ route('testimonial.update', $testimonial) }}" method="post" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control"
                               value="{{ $testimonial->name }}" name="name" autocomplete="name" required  autofocus >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="company_name" class="col-md-4 col-form-label text-md-right">Company Name</label>
                    <div class="col-md-6">
                        <input id="company_name" type="text" class="form-control"
                               name="company_name" value="{{ $testimonial->company_name}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="desc" class="col-md-4 col-form-label text-md-right">Description</label>
                    <div class="col-md-6">
                        <textarea id="desc1" type="text"  class="form-control" name="desc">{{$testimonial->desc}}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-fill btn-primary glyphicon glyphicon-ok"></button>
                    </div>
                </div>
            </form>
        </div>
@endsection
@section('scripts')
    <script>
        CKEDITOR.replace('desc1');
    </script>
@endsection

