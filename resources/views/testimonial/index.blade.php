@extends('layouts.app')

@section('content')
    @foreach($testimonials as $testimonial)
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-title" style="text-align: center;">{{$testimonial->name}}</div>
                    </div>
                    <div class="panel-body">
                        <p>{!! $testimonial->desc !!}</p>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group pull-left">
                            <a href="/admin/testimonial/{{ $testimonial->id }}/edit" class="btn btn-warning btn-circle"><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        </div>
                        <div class="btn-group pull-right">
                            <form action="{{ route('testimonial.destroy', $testimonial) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-circle"><i
                                        class="glyphicon glyphicon-remove"></i></button>
                            </form>
                        </div>
                        <h3 style="text-align: center">{{ $testimonial->company_name }}</h3>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
