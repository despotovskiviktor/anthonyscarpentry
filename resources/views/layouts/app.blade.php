<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet" type="text/css">

    <!-- Page title -->
    <title>Anthony's Carpentry</title>

    <!-- /vendor styles -->
    <link rel="stylesheet" href="/vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/vendor/toastr/toastr.min.css">

    <!-- App styles -->
    <link rel="stylesheet" href="/styles/pe-icons/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/styles/pe-icons/helper.css">
    <link rel="stylesheet" href="/styles/stroke-icons/style.css">
    <link rel="stylesheet" href="/styles/style.css">
    <link rel="stylesheet" href="/dash/assets/css/custom.css">
    <style type="text/css">.jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background:transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }</style>
</head>
<body class="  pace-done">
<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
         style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>

<!-- Wrapper-->
<div class="wrapper">

    <!-- Header-->
    <nav class="navbar navbar-expand-md navbar-default fixed-top">
        <div class="navbar-header">
            <div id="mobile-menu">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
            </div>
            <a class="navbar-brand" href="/admin/home" style="text-align: center">
                ADMIN
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="left-nav-toggle">
                <a href="">
                    <i class="stroke-hamburgermenu"></i>
                </a>
            </div>
        </div>
        <div class="logout">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </nav>
    <aside class="navigation">
        <nav>
            <ul class="nav luna-nav">
                <li>
                    <a href="#extras" data-toggle="collapse" aria-expanded="false" class="pe-7s-user">
                        {{Auth::user()->first_name}} <span class="sub-nav-icon"> <i
                                class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="extras" class="nav nav-second collapse">
                        <li>
                            <a href="/admin/user/{{ $user->id }}/edit"><i class="pe-7s-edit "></i> Edit Profile</a></li>
                        </li>
                    </ul>
                    <hr>
                </li>
                <li class="active">
                    <a href="/admin/home">Dashboard</a>
                </li>
                <li>
                    <a href="#monitoring" data-toggle="collapse" aria-expanded="false"><i class="pe-7s-users"></i>
                        Users<span class="sub-nav-icon"><i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="monitoring" class="nav nav-second collapse">
                        <li><a href="/admin/user">Show Users</a></li>
                        <li><a class="pe-7s-plus" href="/admin/user/create"> Add New User</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#uielements" data-toggle="collapse" aria-expanded="false"><i class="pe-7s-settings"></i>
                        Settings<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="uielements" class="nav nav-second collapse">
                        <li><a href="/admin/settings">Show Settings</a></li>
                        <li><a href="/admin/settings/{{ $settings->id }}/edit"><i class="pe-7s-edit "></i> Edit Settings</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#tables" data-toggle="collapse" aria-expanded="false"><i class="pe-7s-comment"></i>
                        Testimonials<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="tables" class="nav nav-second collapse">
                        <li><a href="/admin/testimonial">Show Testimonials</a></li>
                        <li><a class="pe-7s-plus" href="/admin/testimonial/create"> Create Testimonial</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#forms" data-toggle="collapse" aria-expanded="false"><i class="pe-7s-hammer"></i>
                        Services <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="forms" class="nav nav-second collapse">
                        <li><a href="/admin/categories">Show Services</a></li>
                        <li><a class="pe-7s-plus" href="/admin/categories/create"> Add Service</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#charts" data-toggle="collapse" aria-expanded="false"><i class="pe-7s-photo-gallery"></i>
                        Gallery <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                    </a>
                    <ul id="charts" class="nav nav-second collapse">
                        <li><a href="/admin/gallery">Show Gallery</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </aside>
    <section class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                    <hr>
                </div>
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; <a href="http://www.pingdevs.com">PingDevs</a> {{ \Carbon\Carbon::now()->format('Y') }}</span>
                </div>
            </div>
        </footer>
    </section>
    <!-- End main content-->

</div>
<!-- End wrapper-->


<!-- /vendor scripts -->
<script src="/vendor/pacejs/pace.min.js"></script>
<script src="/vendor/jquery/dist/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendor/toastr/toastr.min.js"></script>
<script src="/vendor/sparkline/index.js"></script>
<script src="/vendor/flot/jquery.flot.min.js"></script>
<script src="/vendor/flot/jquery.flot.resize.min.js"></script>
<script src="/vendor/flot/jquery.flot.spline.js"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAFSgu2HF24dSYTZISe9iGQsvIH954udvs&amp;libraries=places"></script>
<script src="/js/luna.js"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

@yield('scripts')

