<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $settings->title }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Favicon -->

    <meta name="keywords" content="{{ $settings->key_words }}">
    <meta name="description" content="{!! Str::limit(strip_tags($settings->meta_desc), 158) !!}"/>
    <meta name="Author" content="{{ env('APP_NAME') }}"/>
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="{{ $settings->title }}">
    <meta itemprop="description" content="{!! Str::limit(strip_tags($settings->meta_desc), 158) !!}">
    <meta itemprop="image" content="https://anthonyscarpentry.com/assets/img/settings/medium/{{ $settings->meta_img }}">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="{{ env('APP_URL') }}">
    <meta name="twitter:title" content="{{ $settings->title }}">
    <meta name="twitter:description" content="{!! Str::limit(strip_tags($settings->meta_desc), 158) !!}">
    <meta name="twitter:image" content="https://anthonyscarpentry.com/assets/img/settings/medium/{{ $settings->meta_img }}">
    <!-- Open Graph data -->
    <meta property="fb:app_id" content="414881072500201"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="{!! Str::limit(strip_tags($settings->meta_title), 158) !!}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://anthonyscarpentry.com"/>
    <meta property="og:image" content="https://anthonyscarpentry.com/assets/img/settings/medium/{{ $settings->meta_img }}"/>
    <meta property="og:description" content="{!! Str::limit(strip_tags($settings->meta_desc), 158) !!}"/>
    <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>

    <link rel="icon" href="/frontend/assets/img/android-icon-36x36.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700&display=swap" rel="stylesheet">
    <link type="text/css" href="{{ mix('/frontend/assets/css/all.css') }}" rel="stylesheet">

</head>
<body>
<div
    class="header-area header-sticky bg-img space__inner--y40 background-repeat--x background-color--dark d-none d-lg-block"
    data-bg="/frontend/assets/img/icons/ruler.png">
    <!-- header top -->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="header-top-info">
                        <span class="header-top-info__image pr-1"><img src="/frontend/assets/img/icons/phone.png"
                                                                       alt=""></span>
                        <span class="header-top-info__text"><a
                                href="tel:{{ $settings->phone }}">{{ $settings->phone }}</a></span>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="header-top-info text-center">
                        {{ $settings->title }}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="header-top-info text-right">
                        <span class="header-top-info__image pr-1"></span>
                        <span class="header-top-info__text">9.00 am - 18.00 pm</span></div>
                </div>

            </div>
        </div>
    </div>
    <!-- menu bar -->
    <div class="menu-bar position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="menu-bar-wrapper background-color--default space__inner--x35">
                        <div class="menu-bar-wrapper-inner">
                            <div class="row align-items-center">
                                <div class="col-lg-2">
                                    <div class="brand-logo">
                                        <a href="/">
                                            <img src="/assets/img/settings/medium/{{$settings->image}}">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="navigation-area d-flex justify-content-end align-items-center">
                                        <!-- navigation menu -->
                                        <nav class="main-nav-menu">
                                            <ul class="d-flex justify-content-end">
                                                <li>
                                                    <a href="/">Home</a>
                                                </li>
                                                <li><a href="#aboutUs">About Us</a></li>

                                                <li>
                                                    <a href="/#services">Services</a>
                                                </li>
                                                <li class="has-sub-menu">
                                                    <a href="#">Gallery</a>
                                                    <ul class="sub-menu">
                                                        {!! $galleryTree !!}
                                                    </ul>
                                                </li>

                                                <li><a href="/#contact-form">Contact</a></li>
                                            </ul>
                                        </nav>
                                        <!-- search icon nav menu -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of header area  ====================-->
<!--====================  mobile header ====================-->
<div
    class="mobile-header header-sticky bg-img space__inner--y30 background-repeat--x background-color--dark d-block d-lg-none"
    data-bg="assets/img/icons/ruler.png">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6">
                <div class="brand-logo-white">
                    <a href="/">
                        <img src="assets/img/settings/medium/{{$settings->logo_white}}" class="img-fluid"
                             alt="">
                    </a>
                </div>
            </div>
            <div class="col-6">
                <div class="mobile-menu-trigger-wrapper text-right" id="mobile-menu-trigger">
                    <span class="mobile-menu-trigger"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of mobile header  ====================-->
<!--====================  offcanvas mobile menu ====================-->
<div class="offcanvas-mobile-menu" id="mobile-menu-overlay">
    <a href="javascript:void(0)" class="offcanvas-menu-close" id="mobile-menu-close-trigger">
        <span class="menu-close"></span>
    </a>
    <div class="offcanvas-wrapper">
        <div class="offcanvas-inner-content">

            <nav class="offcanvas-navigation">
                <ul>
                    <li class="menu-item-has-children">
                        <a href="/">Home</a>
                    </li>
                    <li class="direct"><a href="/#aboutUs">About Us</a></li>
                    <li class="direct">
                        <a  href="/#services">Services</a>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Gallery</a>
                        <ul class="sub-menu-mobile">
                            {!! $galleryTreeMobile !!}
                        </ul>
                    </li>
                    <li class="direct"><a href="/#contact-form">Contact</a></li>
                </ul>
            </nav>
            <div class="offcanvas-widget-area">
                <div class="off-canvas-contact-widget">
                    <div class="header-contact-info">
                        <ul class="header-contact-info__list">
                            <li><i class="fa fa-clock-o"></i> 9.00 am - 11.00 pm</li>
                        </ul>
                    </div>
                </div>
                <!--Off Canvas Widget Social Start-->
                <div class="off-canvas-widget-social">
                    <a href="{{$settings->facebook}}" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="mailTo:{{$settings->email}}" title="Mail"><i class="fa fa-envelope"></i></a>
                    <a href="tel:{{$settings->phone}}" title="Phone"><i class="fa fa-phone"></i></a>
                </div>
                <!--Off Canvas Widget Social End-->
            </div>
        </div>
    </div>
</div>
<!--====================  End of offcanvas mobile menu  ====================-->
<!--====================  hero slider area ====================-->
<div class="hero-slider-area space__bottom--r120">
    <div class="hero-slick-slider-wrapper">
        <div
            class="single-hero-slider single-hero-slider--background single-hero-slider--overlay position-relative bg-img"
            data-bg="/images/slider1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- hero slider content -->
                        <div class="hero-slider-content hero-slider-content--extra-space">
                            <h2 class="hero-slider-content__title space__bottom--50">We build, install, remodel and
                                repair.
                            </h2>
                            <a href="#contact-form" class="default-btn default-btn--hero-slider">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--====================  End of hero slider area  ====================-->
@yield('content')
<div class="footer-area bg-img space__inner--ry120" data-bg="/images/footer.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="footer-widget">
                    <div class="footer-widget__logo space__bottom--60">
                        <a href="/">
                            <img src="/assets/img/settings/medium/{{ $settings->logo_white }}">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">
                    <h5 class="footer-widget__title space__bottom--20">Useful Links</h5>
                    <ul class="footer-widget__menu">
                        <li><a href="#aboutUs">About Us</a></li>
                        <li><a href="#filters">Our Services</a></li>
                        <li><a href="#services">Our Projects</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-4">
                <h5 class="footer-widget__title space__top--15 space__bottom--20 space__top__md--40 space__top__lm--40"
                    id="footer">
                    Contact us</h5>
                <div class="footer-contact-wrapper">
                    <div class="single-footer-contact">
                        <div class="single-footer-contact__icon"><i class="fa fa-map-marker"></i></div>
                        <div class="single-footer-contact__text">{{ $settings->address }}</div>
                    </div>
                    <div class="single-footer-contact">
                        <div class="single-footer-contact__icon"><i class="fa fa-phone"></i></div>
                        <div class="single-footer-contact__text"><a
                                href="tel:{{ $settings->phone }}">{{ $settings->phone }}</a>
                        </div>
                    </div>
                    <div class="single-footer-contact">
                        <div class="single-footer-contact__icon"><i class="fa fa-envelope"></i></div>
                        <div class="single-footer-contact__text"><a
                                href="mailTo:{{$settings->email}}">{{$settings->email}}</a>
                        </div>
                    </div>
                    <div class="single-footer-contact">
                        <div class="single-footer-contact__icon"><i class="fa fa-facebook"></i></div>
                        <div class="single-footer-contact__text"><a
                                href="{{$settings->facebook}}">{{$settings->facebook}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- copyright text -->
<div class="copyright-area background-color--deep-dark space__inner--y30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p class="copyright-text">&copy; {{ \Carbon\Carbon::now()->format('Y') }} <a
                        href="http://anthonyscarpentry.com.">Anthony's Carpentry</a></p>
            </div>
            <div class="col-lg-12 text-center">
                <p class="powered"> Powered by
                    <a href="http://reshaped.io" style="color: rgba(255,176,35,0.96)">Reshaped Agency LLC</a>
                </p>
            </div>
        </div>
    </div>
</div>
<!--====================  End of footer area  ====================-->
<!--====================  scroll top ====================-->
<button class="scroll-top" id="scroll-top">
    <i class="fa fa-angle-up"></i>
</button>
<!--====================  End of scroll top  ====================-->




<!-- Core -->
<script src="{{ mix('/frontend/assets/js/all.js') }}"></script>


<script>
    lazyload();
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-02SNV1R6QT"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-02SNV1R6QT');
</script>

</body>
</html>
