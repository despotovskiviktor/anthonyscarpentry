<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware' => ['web', 'auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin/home');
    Route::resource('user', App\Http\Controllers\UserController::class);
    Route::resource('settings', App\Http\Controllers\SettingsController::class);
    Route::resource('testimonial', App\Http\Controllers\TestimonialController::class);
    Route::resource('categories', App\Http\Controllers\CategoriesController::class);
    Route::resource('gallery', App\Http\Controllers\GalleryController::class);
    Route::post('gallery/frontpage/{id}', [App\Http\Controllers\GalleryController::class, 'frontpage'])->name('gallery.frontpage');
    Route::get('/categories/{id}/add-gallery',[App\Http\Controllers\CategoriesController::class, 'addGallery'])->name('categories.add-gallery');
});

Route::get('permissions', [App\Http\Controllers\HomeController::class, 'permissions'])->name('permissions');

Route::group(['middleware' => ['web']], function () {
    Route::get('/', [App\Http\Controllers\FrontendController::class, 'index'])->name('public.index');
    Route::get('/{slug}', [App\Http\Controllers\FrontendController::class, 'category'])->name('public.category');
    Route::post('/send-message', [App\Http\Controllers\FrontendController::class, 'sendMessage'])->name('send.message');

});


