$(document).ready(function () {


    $('.isotope-container').fadeIn();
    var $container = $('.isotope-container').isotope({
        itemSelector: '.isotope-item',
        layoutMode: 'fitRows',
        transitionDuration: '0.6s',
        filter: "*"
    });

    // filter items on button click
    $('.filters').on('click', 'ul.nav li button', function (e) {
        e.preventDefault();
        var filterValue = $(this).attr('data-filter');
        $(".filters").find("li.active").removeClass("active");
        $(this).parent().addClass("active");
        $container.isotope({filter: filterValue});


        console.log('Number of images: ' + $(".popup-img").length);
        return false;
    });


    // Magnific popup
    //-----------------------------------------------
    if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) {
        $(".popup-img").magnificPopup({
            type:"image",
            gallery: {
                enabled: true,
            }
        });
        $(".popup-img-single").magnificPopup({
            type:"image",
            gallery: {
                enabled: false,
            }
        });
        $('.popup-iframe').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            preloader: false,
            fixedContentPos: false
        });
    };
});



