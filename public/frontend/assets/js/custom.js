
// Select all links with hashes
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
        ) {

            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();


                var isotop = $(this).attr('data-filter-id');
                trigerIso(isotop);
                 var finalTarget = target.offset().top - 200;
                $('html, body').animate({
                    scrollTop: finalTarget
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });


function trigerIso(id) {
    var $container = $('.isotope-container').isotope({
        itemSelector: '.isotope-item',
        layoutMode: 'fitRows',
        transitionDuration: '0.6s',
        filter: "*"
    });





    var buttonId = "#cat-" + id;


    var filterValue = $(buttonId).attr('data-filter');
    $(".filters").find("li.active").removeClass("active");
    $(buttonId).parent().addClass("active");
    if(buttonId == "#cat-undefined")
    {
        filterValue = '*';
    } else {
        $container.isotope({filter: filterValue});
    }
}


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$("#contact-mail").on("submit", function (e)
{
 e.preventDefault();
 var name = $("input[name=con_name]").val();
 var phone = $("input[name=con_phone]").val();
 var email = $("input[name=con_email]").val();
 var message = $("textarea[name=con_message]").val();

 var sendData = {
     name: name,
     phone: phone,
     email: email,
     message: message
 };
    var formMessages = $('.form-message');
    $.ajax({
        url: "/send-message",
        type: "POST",
        data: sendData,
        dataType: "JSON",
        success: function (data) {

            $(formMessages).removeClass("error");
            $(formMessages).addClass("success");

            if(data.status == "success") {
                $(formMessages).text("Thank you!");
            }


            $("#contact-mail input,#contact-form textarea").val("");
        }
    });


});



