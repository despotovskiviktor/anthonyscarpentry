const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'public/frontend/assets/css/bootstrap.min.css',
    'public/frontend/assets/css/font-awesome.min.css',
    'public/frontend/assets/css/flaticon.min.css',
    'public/frontend/assets/css/plugins/slick.min.css',
    'public/frontend/assets/css/plugins/cssanimation.min.css',
    'public/frontend/assets/css/plugins/justifiedGallery.min.css',
    'public/frontend/assets/css/plugins/light-gallery.min.css',
    'public/frontend/assets/css/vendor.min.css',
    'public/frontend/assets/css/plugins/plugins.min.css',
    'public/frontend/assets/css/style.css',
    'public/frontend/assets/css/magnfic.css',
    'public/frontend/assets/css/custom.css',
], 'public/frontend/assets/css/all.css').version();;


mix.babel([
    'public/frontend/assets/js/modernizr-2.8.3.min.js',
    'public/frontend/assets/js/jquery-3.4.1.min.js',
    'public/frontend/assets/js/bootstrap.min.js',
    'public/frontend/assets/js/plugins/slick.min.js',
    'public/frontend/assets/js/plugins/counterup.min.js',
    'public/frontend/assets/js/plugins/waypoint.min.js',
    'public/frontend/assets/js/plugins/justifiedGallery.min.js',
    'public/frontend/assets/js/plugins/imageloaded.min.js',
    'public/frontend/assets/js/plugins/masonry.min.js',
    'public/frontend/assets/js/plugins/light-gallery.min.js',
    'public/frontend/assets/js/plugins/mailchimp-ajax-submit.min.js',
    'public/frontend/assets/js/isotop.js',
    'public/frontend/assets/js/magnific.js',
    'public/frontend/assets/js/main.js',
    'public/frontend/assets/js/custom.js',
    'public/frontend/assets/js/lazyload.js',
    'public/frontend/assets/js/isotope.pkgd.min.js',

], 'public/frontend/assets/js/all.js').version();;


